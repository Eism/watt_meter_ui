#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "console.h"
#include "settingsdialog.h"

#include <QMessageBox>
#include <QLabel>
#include <QtSerialPort/QSerialPort>
#include <QDebug>
#include <QFile>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    console = new Console;
    console->setEnabled(false);
    //setCentralWidget(console);

    serial = new QSerialPort(this);

    settings = new SettingsDialog;

    ui->actionConnect->setEnabled(true);
    ui->actionDisconnect->setEnabled(false);
    ui->actionQuit->setEnabled(true);
    ui->actionConfigure->setEnabled(true);

    status = new QLabel;
    ui->statusBar->addWidget(status);

    initActionsConnections();

    connect(serial, static_cast<void (QSerialPort::*)(QSerialPort::SerialPortError)>(&QSerialPort::error),
            this, &MainWindow::handleError);

    connect(serial, &QSerialPort::readyRead, this, &MainWindow::readData);
    connect(console, &Console::getData, this, &MainWindow::writeData);

    QDir().mkdir("images");
    QDir().mkdir("log");
}

MainWindow::~MainWindow()
{
    delete settings;
    delete ui;
}

void MainWindow::openSerialPort()
{
    SettingsDialog::Settings p = settings->settings();
    serial->setPortName(p.name);
    serial->setBaudRate(p.baudRate);
    serial->setDataBits(p.dataBits);
    serial->setParity(p.parity);
    serial->setStopBits(p.stopBits);
    serial->setFlowControl(p.flowControl);
    if (serial->open(QIODevice::ReadWrite)) {
        console->setEnabled(true);
        console->setLocalEchoEnabled(p.localEchoEnabled);
        ui->actionConnect->setEnabled(false);
        ui->actionDisconnect->setEnabled(true);
        ui->actionConfigure->setEnabled(false);
        showStatusMessage(tr("Connected to %1 : %2, %3, %4, %5, %6")
                          .arg(p.name).arg(p.stringBaudRate).arg(p.stringDataBits)
                          .arg(p.stringParity).arg(p.stringStopBits).arg(p.stringFlowControl));
    } else {
        QMessageBox::critical(this, tr("Error"), serial->errorString());

        showStatusMessage(tr("Open error"));
    }
}

void MainWindow::closeSerialPort()
{
    if (serial->isOpen())
        serial->close();
    console->setEnabled(false);
    ui->actionConnect->setEnabled(true);
    ui->actionDisconnect->setEnabled(false);
    ui->actionConfigure->setEnabled(true);
    showStatusMessage(tr("Disconnected"));
}

void MainWindow::about()
{
    QMessageBox::about(this, tr("About Simple Terminal"),
                       tr("The <b>Simple Terminal</b> example demonstrates how to "
                          "use the Qt Serial Port module in modern GUI applications "
                          "using Qt, with a menu bar, toolbars, and a status bar."));
}

void MainWindow::writeData(const QByteArray &data)
{
    serial->write(data);
}

void MainWindow::readData()
{
    while (serial->canReadLine()){
        QByteArray data = serial->readLine();
        if (data == "?\n"){
            drawGraph();
            saveDateToFile();
            mData.clear();
        }else
        {
            QList<QByteArray> lineByte = data.replace("\n",0).split(';');
            if (lineByte.length() == 5){
                mData.append(lineByte);
            }

        }
    }
}

void MainWindow::handleError(QSerialPort::SerialPortError error)
{
    if (error == QSerialPort::ResourceError) {
        QMessageBox::critical(this, tr("Critical Error"), serial->errorString());
        closeSerialPort();
    }
}

void MainWindow::initActionsConnections()
{
    connect(ui->actionConnect, &QAction::triggered, this, &MainWindow::openSerialPort);
    connect(ui->actionDisconnect, &QAction::triggered, this, &MainWindow::closeSerialPort);
    connect(ui->actionQuit, &QAction::triggered, this, &MainWindow::close);
    connect(ui->actionConfigure, &QAction::triggered, settings, &MainWindow::show);
    connect(ui->actionClear, &QAction::triggered, console, &Console::clear);
    connect(ui->actionAbout, &QAction::triggered, this, &MainWindow::about);
    connect(ui->actionAboutQt, &QAction::triggered, qApp, &QApplication::aboutQt);
}

void MainWindow::drawGraph()
{
    QVector<double> channelA(mData.length()),
            channelB(mData.length()),
            channelC(mData.length()),
            channelD(mData.length()),
            channelE(mData.length()),
            x(mData.length());
    int l = 0;
    foreach (QList<QByteArray> list, mData) {
        channelA[l] = list[0].toDouble();
        channelB[l] = list[1].toDouble();
        channelC[l] = list[2].toDouble();
        channelD[l] = list[3].toDouble();
        channelE[l] = list[4].toDouble();
        x[l] = l++;
    }

    ui->plot->addGraph();
    ui->plot->graph(0)->setData(x, channelA);
    ui->plot->graph(0)->setPen(QPen(Qt::blue));
    ui->plot->addGraph();
    ui->plot->graph(1)->setData(x, channelB);
    ui->plot->graph(1)->setPen(QPen(Qt::red));
    ui->plot->addGraph();
    ui->plot->graph(2)->setData(x, channelC);
    ui->plot->graph(2)->setPen(QPen(Qt::green));
    ui->plot->addGraph();
    ui->plot->graph(3)->setData(x, channelD);
    ui->plot->graph(3)->setPen(QPen(QColor(153, 102, 0)));
    ui->plot->addGraph();
    ui->plot->graph(4)->setData(x, channelE);
    ui->plot->graph(4)->setPen(QPen(Qt::black));

    ui->plot->xAxis->setLabel("x");
    ui->plot->yAxis->setLabel("y");

    ui->plot->xAxis->setRange(0, mData.length()+5);
    ui->plot->yAxis->setRange(0, 4000);
    ui->plot->replot();

    ui->plot->savePng("images/plot" + QString::number(k++) + ".png");

}

void MainWindow::saveDateToFile()
{
    QFile file("log/app.log");
    if (!file.open(QIODevice::WriteOnly | QIODevice::Append))
        return;

    foreach (QList<QByteArray> list, mData) {
        file.write(list[0] + ';' +
                list[1] + ';' +
                list[2] + ';' +
                list[3] + ';' +
                list[4] + '\n');
    }
    file.write("?\n");
    file.close();
}

void MainWindow::showStatusMessage(const QString &message)
{
    status->setText(message);
}
